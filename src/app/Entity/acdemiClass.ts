import { Grade } from './grade';
import { Period } from './period';

export class AcademiClass {
  public id: string ;
  public name: string ;
  public number: string ;
  public grade: Grade = new Grade;
  public period: Period = new Period;
  public year: string;

  constructor() {}
}
