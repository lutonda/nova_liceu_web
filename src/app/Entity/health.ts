class Health {
  id: string;
  bloodGroup: BloodGroup;
  specialCare: string;
  allergies: string;
}
