class Person {
  id: string;
  firstName: string;
  middleName: string;
  lastName: string;
  sex: Sex;
  idCard: IdCard;
  contacts: Array<Contact>;
  adress: Adress;
  health: Health;
  birthDate: Date;
}
